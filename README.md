# Chatbot Proxy

Main cog of the Smartbots Architecture. It handles requests from different inputs (web and facebook so far) and matches with the NLP engine. See picture below

 ![alt text](bot_architecture.png)

## Getting Started

To run this project in development first install RVM (ruby version manager (it is like virtualenv for python))

```
curl -sSL https://get.rvm.io | bash -s stable --ruby
rvm install ruby-2.4.0
gem install bundle
rvm use 2.4.0@chatbot-proxy --create
```

### Prerequisites

In order to test hooks from external services, like incoming messages from Facebook you are going to need ngrok "Secure tunnels to localhost"

Once installed on your machine run the following to map to localhost:3000

```
./ngrok http 3000
```

### Installing

```
bundle install
```

## Running the tests

```
rspec
```

## Generating documentation

```
rails clobber_rdoc  # Remove RDoc HTML files
rails rdoc          # Build RDoc HTML files
rails rerdoc        # Rebuild RDoc HTML files
```

### Coverage with simple cov

find the coverage report after running your specs on ./coverage/index.html


### And coding style tests

Coding style ruled by Rubocop

```
gem install rubocop
```

## Deployment

pendding


## Authors

* **Sebastian Schuchhardt**
* **Martin Villalva**
* **Sebastian Jimenez**
