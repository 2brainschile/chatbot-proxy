# This controller handles incomming messages form facebook messenger platform
# specially for the customer chat plugin
# https://developers.facebook.com/docs/messenger-platform/discovery/customer-chat-plugin
# It has only two endpoints index for register the application, and create
# to create new messages
class Messenger::WidgetController < ApplicationController
  def index
    service_response = handler_service.register_facebook_hook(register_params)
    render json: service_response[:body], status: service_response[:status]
  end

  def create
    @sender_id = message_params[:entry][0][:messaging][0][:sender][:id]
    service_response = handler_service.handle_new_facebook_message(message_params)
    render json: service_response[:body], status: service_response[:status]
  end

  private

# This method creates a handler, which creates the other service objects
# the important one is ApiaiService::Client, which takes an object with options
# the session id for api ai to track contexts is the sender id
  def handler_service
    HandlerService::Handler.new(build_id, api_session_id: @sender_id)
  end

  def build_id
    return register_params['hub.verify_token'] if register_params.values.any?
    message_params['entry'].first['id']
  end

# This method gets the parameters needed to regestry the hook on facebook.
# Facebook does a call to index(GET request) with this parametes
  def register_params
    params.permit('hub.challenge', 'hub.mode', 'hub.verify_token')
  end

  def message_params
    params.permit(
      entry: [
        :id,
        :time,
        messaging: [
          sender: [:id],
          recipient: [:id],
          postback: %i[payload title timestamp time],
          message: %i[mid seq text]
        ]
      ]
    ).merge(params.permit(:object))
  end
end
