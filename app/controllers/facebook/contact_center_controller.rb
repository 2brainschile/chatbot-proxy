# This controller handles request from the contact center application and sends
# messages to client in Facebook Messenger
class Facebook::ContactCenterController < ApplicationController
  def create
    # services_response ontains responses from both facebook and firebase
    # only facebook response is considered for now
    services_response = handler_service.handle_new_manual_message(message_params)
    render json: services_response[:facebook][:body],
           status: services_response[:facebook][:status]
  end

  private

  def handler_service
    HandlerService::Handler.new(build_id)
  end

  def build_id
    message_params['fanpage_id']
  end

  def message_params
    params.permit(:fanpage_id, :user_id, messages: %i[speech type imageUrl])
  end
end
