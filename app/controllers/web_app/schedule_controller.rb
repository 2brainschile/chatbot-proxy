# This controller handles request from the contact center application and sends
# messages to client in Facebook Messenger
class WebApp::ScheduleController < ApplicationController
  
    def working
      service_response = working?
      render json: {working: service_response}, status: 200
    end
  
    private
  
     def working?
      return false if job_time.nil? || !job_time['enabled']
      check_day_and_time
    end

    def check_day_and_time
      day = current_time[:date][:day_of_the_week].capitalize.tr("áéíóúÁÉÍÓÚ", "aeiouAEIOU").gsub(" ", "")
      time = current_time[:time].to_time

      am_range = {
                  startAt: job_time['days'][day]['am']['startAt'],
                  endAt: job_time['days'][day]['am']['endAt']
                }
      pm_range = {
                  startAt: job_time['days'][day]["pm"]['startAt'],
                  endAt: job_time['days'][day]["pm"]['endAt']
                }
      return time_eval(am_range, time) || time_eval(pm_range, time)
    end

    def time_eval(range, time)
      return false if range[:startAt].nil? or range[:startAt].empty? or range[:endAt].nil? or range[:endAt].empty?
      (range[:startAt].to_time < time) && (time < range[:endAt].to_time)      
    end

    def current_time
      TimeService::Client.new.global_date_time('Chile')
    end

    def job_time
      firebase = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
      firebase.get("schedules/#{params[:chatbot_id]}").body
    end
  
    def schedule_params
      params.permit(:chatbot_id)
    end
  end
