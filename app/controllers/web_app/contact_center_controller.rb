# This controller handles request from the contact center application and sends
# messages to client in Facebook Messenger
class WebApp::ContactCenterController < ApplicationController
    def create
      service_response = handler_service.forward_message_to_facebook(message_params)
      render json: service_response[:body], status: service_response[:status]
    end
  
    def broadcast
      test_service = handler_service.broadcast_message(broadcast_params)
      render json: {}, status: 200 # TODO: Status?
    end

    def notification
      test_service = handler_service.notification_message(notification_params)
      render json: {}, status: 200 # TODO: Status?
    end

    def report
      test_service = handler_service.generate_report(report_params)
      render json: {}, status: 200 # TODO: Status?
    end

    private
  
    def handler_service
      HandlerService::Handler.new(build_id)
    end
  
    def build_id
      message_params['chatbot_id']
    end
  
    def message_params
      params.permit(:chatbot_id, :chat_id, :message)
    end

    def notification_params
      params.permit(:chatbot_id, :message, :title, :image_url, :link)
    end

    def broadcast_params
      params.permit(:chatbot_id, :message, recipents:[:id, :origin, :fanpage_id, :user_name])
    end

    def report_params
      params.permit(:chatbot_id)
    end
  end
