# This controller handles request from the contact center application and sends
# messages to client in Whatsapp
class Whatsapp::ContactCenterController < ApplicationController
  def create
    services_response = handler_service.handle_new_manual_message_in_whatsapp(message_params.merge({senderName: 'cc'}))
    render json: services_response[:body],
           status: services_response[:status]
  end

  def notification
        services_response = handler_service.handle_new_manual_message_in_whatsapp(message_params.merge({senderName: 'API'}))
    render json: services_response[:body],
           status: services_response[:status]
  end

  private

  def handler_service
    HandlerService::Handler.new(build_id)
  end

  def build_id
    message_params['chatbot_id']
  end

  def message_params
    params.require(:contact_center).permit(:chatbot_id, :user_id, :message, :bot_whatsapp_id)
  end
end
