# Handles messages from whatsapp
class Whatsapp::MessagesController < ApplicationController
  def create
    service_response = handler_service.handle_new_whatsapp_message(message_params)
    render json: service_response[:body], status: service_response[:status]
  end

  def create
    service_response = handler_service.handle_new_whatsapp_message(message_params)
    render json: service_response[:body], status: service_response[:status]
  end

  private

  def message_params
    params.permit(:message, :user, :phone, :chatbot_id, :user_id, :img)
  end
  
  def handler_service
    api_ai_opts = { api_session_id: message_params[:user_id] }
    HandlerService::Handler.new(message_params[:chatbot_id], api_ai_opts)
  end

end
