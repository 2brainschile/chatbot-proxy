class Settings::IntentsController < ApplicationController
  def index
    service_response = FirebaseWrapper.new.get_all_default_intent
    render json: service_response.body, status: service_response.code
  end

  def show
    service_response = FirebaseWrapper.new.get_default_intent(params[:id])
    render json: service_response.body, status: service_response.code
  end

  def create
    service_response = FirebaseWrapper.new.create_default_intents(intent_params)
    render json: service_response.body, status: service_response.code
  end

  def update
    service_response = FirebaseWrapper.new.update_default_intent(intent_params)
    render json: service_response.body, status: service_response.code
  end

  def destroy
    service_response = FirebaseWrapper.new.delete_default_intent(params[:id])
    render json: service_response.body, status: service_response.code
  end

  private

  def intent_params
    params.require(:intent).permit(:id, :name, answers: [:description], intents: [:description])
  end
end
