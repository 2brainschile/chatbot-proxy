class Komatsu::TicketsController < ApplicationController
  def create
    service_response = CaService::Client.new.perform_request(service_params)
    render json: service_response[:body], status: service_response[:status]
  end

  private
  def tickets_params
    params.permit(
      result: { 
        parameters: [:ticket_id, :user_email, :type_message, :ticketSummary, :ticketDescription],
        metadata: :intentName
      }
    )
  end

  def service_params
    {
      ticket_id: tickets_params["result"]["parameters"]["ticket_id"],
      user_email: tickets_params["result"]["parameters"]["user_email"],
      type_message: tickets_params["result"]["parameters"]["type_message"],
      intent_name: tickets_params["result"]["metadata"]["intentName"],
      ticket_summary: tickets_params["result"]["parameters"]["ticketSummary"],
      ticket_description: tickets_params["result"]["parameters"]["ticketDescription"]
    }
  end
end