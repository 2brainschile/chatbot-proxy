class ChromeExt::MessagesController < ApplicationController
  def create
    service_response = handler_service.handle_new_chrome_ext_message(message_params)
    render json: service_response[:body], status: service_response[:status]
  end

  private

  def message_params
    params.permit(:chatroom_id,
                  :message,
                  :chatbot_id,
                  :api_session_id,
                  :ignore,
                  :api_lang)
  end

  def handler_service
    api_ai_opts = { api_session_id: message_params[:chatroom_id],
                    api_lang:       message_params[:api_lang] }
    HandlerService::Handler.new(message_params[:chatbot_id],
                                api_ai_opts, false)
  end
end
