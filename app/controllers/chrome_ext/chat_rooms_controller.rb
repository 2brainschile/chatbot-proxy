class ChromeExt::ChatRoomsController < ApplicationController

  def create
    # TODO: Auth user
    service_response = handler_service.handle_new_chrome_ext_chatroom(chatroom_params)
    render json: { chat_id: service_response[:body] }, status: service_response[:status]
  end

  private

  def chatroom_params
    params.require(:chat_room).permit(:chatbot_id,
                                      :user_email,
                                      :user_name,
                                      :user_phone,
                                      :api_lang)
  end

  def handler_service
    api_ai_opts = { api_session_id: guess_chatroom_id,
                    api_lang:       chatroom_params[:api_lang] }
    HandlerService::Handler.new(chatroom_params[:chatbot_id], api_ai_opts)
  end

  def guess_chatroom_id
    chatroom_params[:user_email].gsub('.', ',')
  end
end
