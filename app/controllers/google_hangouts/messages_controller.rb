class GoogleHangouts::MessagesController < ApplicationController
  def create
    set_params
    service = GoogleHangoutsService::Client.new
    render json: service.response(@hangouts_id, @message, @email), status: 200
  end

  private
  def check_card_message
    begin
      card_response[:card_option][:parameters].first[:value]
    rescue
      message_params[:msg][:message][:text]
    end
  end

  def card_response
    params.permit(card_option: [ parameters: [:value]])
  end

  def message_params
    params.permit( msg: [
        message: [ :text, space: [:name] ], user: [:email]]
    )
  end

  def set_params
    @hangouts_id = message_params[:msg][:message][:space][:name]
    @message     = check_card_message
    @email = message_params[:msg][:user][:email]
  end
end
