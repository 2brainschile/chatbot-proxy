class Statistics::BotStatisticsController < ApplicationController
  def show
    render json: StatisticsService::ChatbotStatistics.new(params[:id]).build_bot_statistics, status: 200
  end
end