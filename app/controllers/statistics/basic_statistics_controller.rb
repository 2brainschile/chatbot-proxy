class Statistics::BasicStatisticsController < ApplicationController
  def show
    render json: StatisticsService::ChatbotStatistics.new(params[:id]).build_basic_statistics, status: 200
  end
end