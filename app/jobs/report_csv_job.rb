class ReportCsvJob < ApplicationJob
  queue_as :default
  
  def perform(*args)
    @firebase_client = FirebaseWrapper.new
    chatbot_id = args[0]
    chats = @firebase_client.get("chats/#{chatbot_id}")
    if chats.success?
      append_conversation_headers chatbot_id
      chats.body.each do |chat_id, chat|
        chatroom = @firebase_client.get("chatrooms/#{chatbot_id}/#{chat_id}")
        chatroom_detail = chatroom.body
        next if chatroom_detail.nil?
        user_detail = {
          id: chatroom_detail['userId'],
          name: chatroom_detail['userName'],
          email: chatroom_detail['userEmail'],
          phone: chatroom_detail['userPhone'],
          origin: chatroom_detail['origin'],
        }
        # append_conversation_detail user_detail, chatroom_detail['createdAt']
        if chat["messages"] && !tests_emails.include?(chatroom_detail[:email])
          chat["messages"].each do |conversation_id, conversation|
            if conversation['message']['speech'] && !conversation['message']['ignore']
              #puts "#{conversation['sender']}: #{conversation['message']['speech']}"
              message = conversation['message']['speech']
            else
              #puts "#{conversation['sender']}: #{conversation['message']}"
              message = conversation['message']
            end
            if message['payload'] || message['type']
              if message['payload'] && message['payload']['custom_message']
                message = message['payload']['custom_message']['text']
              else
                next
              end
            end
            if not message.is_a?(String)
              next
            end

            messageDate = conversation['createdAt'] || conversation['sendAt']
            messageDate = Time.at((messageDate/1000)).to_datetime.strftime('%I:%M%p %d/%m/%Y')
            conversationDate = Time.at((chatroom_detail['createdAt']/1000)).to_datetime.strftime('%I:%M%p %d/%m/%Y')
            append_conversation_to_csv chatbot_id, conversation['sender'], message, user_detail, messageDate, conversationDate
          end
        end
      end
    end
  end

  def append_conversation_headers chatbot_id
    CSV.open("conversations_#{chatbot_id}.csv", "a+") do |csv|
        csv << ["ID", "NOMBRE", "CORREO", "FONO", "ORIGEN", "FECHA CREACION", "EMISOR", "FECHA/HORA CONVERSACION", "TEXTO"]
    end
  end

  def append_conversation_to_csv chatbot_id, sender, text, user, message_date, createdAt
    CSV.open("conversations_#{chatbot_id}.csv", "a+") do |csv|
      csv << [user[:id], user[:name], user[:email], user[:phone], user[:origin], createdAt, sender, message_date, text]
    end
  end

end
