class BroadcastMessageJob < ApplicationJob
  queue_as :default
  
  def perform(*args)
    @firebase_client = FirebaseWrapper.new
    chatbot_id = args[0]
    message    = args[1]
    recipients  = args[2] # is a hash

    recipients.each do |recipent|
      recipent_id = recipent[:id]
      origin      = recipent[:origin]
      @firebase_client.add_message(chatbot_id, recipent_id, build_message(message))
      if origin == 'facebook'
        fanpage_id = recipent[:fanpage_id]
        handle_facebook(fanpage_id, recipent_id, message)
      end
    end
    @firebase_client.add_broadcast_message(chatbot_id, recipients, build_message(message))
  end

  def build_message(message)
    message_hash = { createdAt: Firebase::ServerValue::TIMESTAMP,
                     body:      message,
                     sender:    'contact-center',
                     ignore:    false }
  end

  def handle_facebook(fanpage_id, recipent_id, message)
    @bot_name = @firebase_client.fetch_bot_name(fanpage_id)
    services = @firebase_client.fetch_services(fanpage_id) || {}
    facebook_client = FacebookService::Client.new(services['facebook'])
    
    message_hash = { type: 0, speech: message }
    fb_opts = [fanpage_id, recipent_id, message_hash]
    fb_params = facebook_client.build_contact_center_msg_obj(*fb_opts)
    facebook_client.send_direct_message(fb_params)
  end
end
