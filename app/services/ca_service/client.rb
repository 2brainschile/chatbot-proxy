require 'ca_service/access_token'
require 'ca_service/tickets'
require 'ca_service/request'
require 'ca_service/users'

module CaService
  class Client
    include CaService::AccessToken
    include CaService::Tickets
    include CaService::Users

    attr_accessor :token

    def perform_request(params)
      begin
        case params[:intent_name]
        when 'can_do.query_tickets'
          # Intent: mostrar ultimos tickets
          resp = get_all_tickets(token)
        when 'can_do.query_tickets.id','can_do.query_tickets.two_steps'
          # informacion del ticket 520016
          # update Iann Yanes para resolver el Ticket No. SMAR-51
          # FIN update Iann Yanes para resolver el Ticket No. SMAR-51
          resp = get_ticket(token, params[:ticket_id])
        when 'can_do.query_tickets.user'
          # tickets de jorge.abalos@Ca.cl
          resp = get_ticket_by_email(token, params[:user_email])
        when 'can_do.tkt_sys.create'
          resp = create_ticket(token, params[:ticket_summary], params[:ticket_description])
        end
        success_response(resp)
      rescue
        error_response
      end
    end

    def token
      @token ||= get_token
    end

    def success_response(response)
      {
        body: { speech: response },
        status: 200
      }
    end

    def error_response
      {
        body: { speech: 'Ocurrio un error con el servicio de tickets'},
        status: 500
      }
    end
  end
end

# CaService::Client.new.perform_request({})
# CaService::Client.new.get_token
# CaService::Client.new.get_all_tickets()
# CaService::Client.new.get_all_tickets('437404771')
