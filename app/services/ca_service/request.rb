require 'uri'
require 'net/http'
require 'openssl'

module CaService
  class Request
    BASE_URL = 'https://webintegration.sonda.com:8050/caisd-rest'
    # BASE_URL = 'https://desintegration.sonda.com:8050/caisd-rest'

    attr_accessor :http, :service, :params, :headers, :request

    def initialize(method_request, service, path, params = {}, headers = {})
      @method_request = method_request
      @service = service
      @params = params
      @headers = headers
      url = URI(BASE_URL + path)
      @http = Net::HTTP.new(url.host, url.port)
      @http.use_ssl = true
      @http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      @request = build_request_by_method!(url)
    end

    def build_request_by_method!(url)
      case @method_request
      when :post
        Net::HTTP::Post.new(url)
      when :get
        Net::HTTP::Get.new(url)
      end
    end

    def load_headers_common!
      @request["content-type"] = 'application/xml'
      @request["cache-control"] = 'no-cache'
    end

    def load_basic_authentication!
      # @request.basic_auth("integ_kcl", "Sonda.2019")
      @request.basic_auth("integ_kcl", "KCL.2019")
    end

    def load_headers!
      load_headers_common!
      return if @headers.empty?
      @headers.each do |key, value|
        @request[key] = value
      end
    end

    def load_body!
      @request.body = @params[:body]
    end

    def load_actions!
      load_headers!
      load_body!
      load_basic_authentication! if @service == :access_token
    end

    def perform
      load_actions!
      response = @http.request(@request)
      Nokogiri::Slop response.read_body
    end
  end
end
