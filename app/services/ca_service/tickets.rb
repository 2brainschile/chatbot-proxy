module CaService
  module Tickets
    def tickets_headers(access_token)
      {
        :"X-AccessKey" => access_token,
        :"X-Obj-Attrs" => 'ref_num, status, summary, description, assignee, affected_contact, requestor, requested_by, log_agent'
      }
    end

    def body_to_create_a_ticket(summary, description, uid)
      {
        body:
        "<in>
          <customer REL_ATTR='#{uid}'/>
          <requested_by REL_ATTR='#{uid}'/>
          <summary>#{summary}</summary>
          <description>#{description}</description>
          <category REL_ATTR='pcat:1121115027'/>
          <zreporting_met REL_ATTR='7302'/>
        </in>"
      }
    end

    def create_ticket(access_token, summary, description, email, uid = nil)
      uid = get_user_by_email(token, email) unless uid

      response = CaService::Request.new(
        :post,
        :tickets,
        '/in',
        body_to_create_a_ticket(summary, description, uid),
        tickets_headers(access_token)
      ).perform

      ticket_id = response.document.in["COMMON_NAME"]
      link = response.document.in.link['href']
      "Tu ticket ha sido creado, su ID es #{ticket_id}"
    end

    def get_ticket_by_email(access_token, user_email)
      response = CaService::Request.new(
        :get,
        :tickets,
        "/cr?WC=customer.userid%3D'#{user_email}'%20AND%20status%20%3C%3E%20%27CL%27%20AND%20status%20%3C%3E%20%27RE%27%20AND%20status%20%3C%3E%20%27CNCL%27",
        {},
        tickets_headers(access_token)
      ).perform

      build_array_and_msgs_response(response.document.collection_cr.cr)
    end

    def get_ticket(access_token, ticket_id)
      response = CaService::Request.new(
        :get,
        :tickets,
        "/cr?WC=ref_num%3D'#{ticket_id}'",
        {},
        tickets_headers(access_token)
      ).perform
      build_simple_message(response)
    end

    def get_all_tickets(access_token)
      response = CaService::Request.new(
        :get,
        :tickets,
        '/in?WC=status%20%3C%3E%20%27CL%27%20AND%20status%20%3C%3E%20%27RE%27%20AND%20status%20%3C%3E%20%27CNCL%27',
        {},
        tickets_headers(access_token)
      ).perform
      build_array_and_msgs_response(response.document.collection_in.in)
    end

    def build_array_and_msgs_response(resp)
      tickets = build_array_to_tickets(resp)
      build_messages(tickets)
    end

    def build_array_to_tickets(tickets)
      begin
        tickets.each_with_object([]) do |ticket, rtn| 
          element = {
            id: ticket["COMMON_NAME"],
            status: ticket.status["COMMON_NAME"],
            summary: ticket.summary.content,
          }

          element.merge({assignee: ticket.assignee["COMMON_NAME"]}) if ticket.try(:assignee)
          rtn << element
        end
      rescue
        []
      end
    end

    def no_tickets_error
      {
        card: {
          error: true,
          message_error: "El usuario no tiene tickets abiertos."
        }
      }.to_json
    end

    # tickets: Array[ Hash {ticket_id, status} ]
    def build_messages(tickets)
      return no_tickets_error if tickets.empty?

      items = tickets.each.inject([]) do |rtn, ticket|
        rtn << [ticket[:id], ticket[:status]]
      end

      {
        "card":
          { "header": "Información de los tickets",
            "body": {
              "description": "El estado de los tickets es el siguiente", 
              "normalList": {
                "headers": ["ID", "Estado"],
                "items": items
              }
            }
          }
      }.to_json
    end

    def no_ticket_error
      {
        card: {
          error: true,
          message_error: "No se encuentra el ticket. Por favor verifique que haya escrito el ID correcto e intente de nuevo."
        }
      }.to_json
    end

    def build_simple_message(ca_response)
      begin
        ticket = ca_response.document.collection_cr.cr
      rescue
        return no_ticket_error
      end
      ticket_id = ticket["COMMON_NAME"]
      assignee_name = ticket.try(:assignee) ? ticket.assignee["COMMON_NAME"] : '-'
      requested_by_name = ticket.requested_by["COMMON_NAME"]
      status = ticket.status["COMMON_NAME"]
      summary = ticket.summary.content

      {
        "card":
          { "header": "Información del ticket",
            "body": {
              "title": "##{ticket_id}",
              "description": "#{summary}", 
              "specialList": [
                {"title_item": "Estado", "content": "#{status}"},
                {"title_item": "Asignado a", "content": "#{assignee_name}"}
              ]
            }
          }
      }.to_json
    end
  end
end