module CaService
  module AccessToken
    def token_body
      { body: '<rest_access/>' }
    end

    def get_token
      res = CaService::Request.new(:post, :access_token, '/rest_access', token_body).perform
      res.document.rest_access.access_key.content
    end
  end
end