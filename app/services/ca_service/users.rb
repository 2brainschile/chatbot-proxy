module CaService
  module Users
    def user_headers(access_token)
      {
        :"X-AccessKey" => access_token
      }
    end

    def get_user_by_username(token, username)
      res = CaService::Request.new(:get, :users, "/cnt?WC=userid%3D'#{username}'", {},user_headers(token)).perform
      begin
        res.document.collection_cnt.cnt['id'].split('\'').last
      rescue
        #'C8BD9B61B7F046478E58C27B01AE6254' staging
        #'8B3071F775896A459FC0D220D449625E' produccion
        '8B3071F775896A459FC0D220D449625E'
      end
    end

    def get_user_by_email(token, email)
      res = CaService::Request.new(:get, :users, "/cnt?WC=email_address%3D'#{email}'", {},user_headers(token)).perform
      begin
        res.document.collection_cnt.cnt['id'].split('\'').last
      rescue
        #'C8BD9B61B7F046478E58C27B01AE6254' staging
        #'8B3071F775896A459FC0D220D449625E' produccion
        '8B3071F775896A459FC0D220D449625E'
      end
    end
  end
end