
module TimeService
  class Client

    def global_date_time(country = 'Chile')
      page = HTTParty.get("https://time.is/es/#{country}")
      parse_page = Nokogiri::HTML(page)
      date_object =  {}
      date_object[:time] = parse_page.css('#twd').inner_html if parse_page.css('#twd')
      date_object[:date] = extract_dates(parse_page)
      date_object[:country] = country
      date_object.compact
    end

    def extract_dates(parse_page)
      return nil unless parse_page.css('#dd')
        keys = [:day_of_the_week, :month_and_day_num, :year, :week_num]
        values = parse_page.css('#dd').inner_html.split(',')
        Hash[keys.zip values]
    end    
  end
end
