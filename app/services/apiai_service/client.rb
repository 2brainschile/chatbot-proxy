
module ApiaiService
  class Client
    attr_accessor :api_ai_client
    attr_reader :key

    def initialize(service_data, api_opts = {})
      service_data = service_data || {}
      keys         = service_data[:keys] || service_data['keys'] || {}
      @key         = keys[:API_AI_ID] || keys['API_AI_ID']

      api_opts[:api_lang] = 'ES' if api_opts[:api_lang].nil?
      opts = { client_access_token: key,
               api_base_url: 'https://api.api.ai/v1/',
               api_version: '20150910',
               timeout_options: [:global, { write: 30, connect: 30, read: 30 }] }
      opts = opts.merge(api_opts)
      @api_ai_client = ApiAiRuby::Client.new(opts)
    end

    # This method returns the hash when default intent is triggered
    def default_welcome(message, user_name = nil)
      if api_ai_client.api_lang != "ES"
        req = HTTParty.get("#{dialog_base_url}/query?v=20150910&lang=en&e=#{message}&sessionId=#{api_ai_client.api_session_id}", headers: dialogflow_header)
        resp = req.parsed_response.deep_symbolize_keys!
      else
        resp = api_ai_client.event_request(message, { userName: user_name }, resetContexts: true)
      end
      reponse_status(resp)
    end

    def extract_messages(raw_response)
      filtered_for_facebook_platform(raw_response[:body])
    end

    def send_message(message)
      restricted_message = message.length <= 256 ? message : message[0..200]
      if api_ai_client.api_lang != "ES"
        req = HTTParty.get("#{dialog_base_url}/query?v=20150910&lang=en&q=#{message}&sessionId=#{api_ai_client.api_session_id}", headers: dialogflow_header)
        resp = req.parsed_response.deep_symbolize_keys!
      else
        begin
          resp = api_ai_client.text_request(restricted_message)
        rescue ApiAiRuby::RequestError => e
          puts e.message
          puts e.backtrace.inspect
          resp = JSON.parse(File.read(Rails.root.join("app/services/apiai_service/hook_response.json"))).to_h
        end
      end
      reponse_status(resp)
    end

    def reponse_status(response)
      response.deep_symbolize_keys!
      if response[:status][:code] == 200
        { body: response, status: 200 }
      else
        { body: response, status: response[:status][:code] }
      end
    end

    def dialogflow_header
      {
        'Authorization': "Bearer #{key}",
        'Content-Type': 'application/json; charset=utf-8'
      }
    end

    def dialog_base_url
      "https://api.dialogflow.com/v1"
    end

    def filtered_for_facebook_platform(response_result)
      if has_platform_key?(response_result)
        response_result[:result][:fulfillment][:messages].select do |hash|
          hash[:platform] == "facebook"
        end
      else
        response_result[:result][:fulfillment][:messages]
      end
    end

    def has_platform_key?(response_result)
      counter = 0
      if response_result[:result] && response_result[:result][:fulfillment] && response_result[:result][:fulfillment][:messages]
        response_result[:result][:fulfillment][:messages].each do |hash|
          if hash.keys.include?(:platform) then counter += 1 end
        end
      end
      counter > 0 ? true : false
    end

  end
end
