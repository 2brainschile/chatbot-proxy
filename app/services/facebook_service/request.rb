require 'uri'
require 'net/http'

module FacebookService
  class Request
    BASE_URI = 'https://graph.facebook.com/v2.6/me'

    def perform_post_request(site, opts)
      url = URI("#{BASE_URI}#{site}")

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Post.new(url)
      request["content-type"] = 'application/json'
      request["cache-control"] = 'no-cache'
      request.body = opts[:body]
      puts request.inspect
      puts url
      http.request(request)
    end
  end
end
