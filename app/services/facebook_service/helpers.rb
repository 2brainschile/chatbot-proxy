module FacebookService
  module Helpers
    def user_name(user_id)
      profile_info = HTTParty.get("https://graph.facebook.com/v2.10/#{user_id}?fields=name&access_token=#{page_access_token}")
      return profile_info.parsed_response['name']
    end

    def fanpage_id
      HTTParty.get("https://graph.facebook.com/v2.10/me?fields=id&access_token=#{page_access_token}").parsed_response['id']
    end

    def first_message?(msg)
      ['GET_STARTED_PAYLOAD', "FACEBOOK_WELCOME", "getstarted", "USER_DEFINED_PAYLOAD"].include? msg['postback']
    end
  end
end
