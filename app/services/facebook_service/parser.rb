module FacebookService
  module Parser
    def simplify_message(message_params={})
      msg_obj = {}
      if message_params['object'] == 'page'
        message_params['entry'].each do |entry|
          msg_obj['fanpage_id'] = entry['id']
          msg_obj['timeOfMessage'] = entry['time']
          entry['messaging'].each do |message|
            msg_obj['sender'] = message['sender']['id']
            msg_obj['message'] = message['message']
            msg_obj['read'] = message['read']
            msg_obj['postback'] = message['postback']['payload'] if message['postback']
            msg_obj['title'] = message['postback']['title'] if message['postback']
            return msg_obj
          end
        end
      else
        msg_obj['error'] = "No 'object' in params"
      end
    end

    def build_contact_center_msg_obj(fanpage_id, user_id, message)
      if message[:type].to_s == '0'
        { fanpage_id: fanpage_id, sender: user_id, message: message[:speech] }.with_indifferent_access
      elsif message[:type].to_s == '3'
        new_message = { "attachment": { "type": "image", "payload": { "url": message[:imageUrl] } } }
        { fanpage_id: fanpage_id, sender: user_id, message: new_message }.with_indifferent_access
      end
    end
  end
end
