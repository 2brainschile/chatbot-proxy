# This module provides basic integration to Facebook Messenger.
# Sends direct messages and multiple messages to a specific user
# It also sets up the message formats before posting to Facebook
module FacebookService
  module Messages
    def send_direct_message(fb_params)
      hash = build_json(fb_params[:sender], fb_params[:message])
      post_to_facebook("/messages?access_token=#{page_access_token}", hash)
    end

    def send_mutiple_messages(sender_id, messages)
      resp = {}
      messages.each do |message|
        hash = build_json(sender_id, message)
        resp = post_to_facebook("/messages?access_token=#{page_access_token}", hash)
      end

      { body: resp.body, status: resp.code.to_i }
    end

    def post_to_facebook(uri, options)
      FacebookService::Request.new.perform_post_request(uri, options)
    end

    def build_json(sender_id, message)
      {
        # query: { access_token: page_access_token },
        headers: { 'Content-Type': 'application/json' },
        body: {
          messaging_type: "RESPONSE",
          recipient: { "id": sender_id },
          message: build_message(message)
        }.to_json
      }
    end

    def build_message(message)
      return { "text": message } if message.is_a?(String)
      message.to_json
    end

    def message_parser(response_result)
      messages = []
      response_result = filtered_for_facebook_platform(response_result)
      response_result.each do |msg|
        messages << format_based_on_type(msg)
      end
      messages
    end

    def filtered_for_facebook_platform(response_result)
      if has_platform_key?(response_result)
      response_result[:result][:fulfillment][:messages].select do |hash|
          hash[:platform] == "facebook"
        end
      else
        response_result[:result][:fulfillment][:messages]
      end
    end

    def has_platform_key?(response_result)
      counter = 0
      response_result[:result][:fulfillment][:messages].each do |hash|
        if hash.keys.include?(:platform) then counter += 1 end
      end
      counter > 0 ? true : false
    end

    def format_based_on_type(msg)
      case msg[:type]
       when 3
        { 'attachment': {'type': 'image','payload': {"url": msg[:imageUrl]}}}
      when 4
        msg[:payload][:facebook]
      when 0
        msg[:speech]
      else
        msg[:speech]
      end
    end
  end
end
