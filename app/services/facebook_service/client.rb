require 'facebook_service/messages'
require 'facebook_service/request'
require 'facebook_service/parser'
require 'facebook_service/helpers'

module FacebookService
  class Client
    include FacebookService::Helpers
    include FacebookService::Messages
    include FacebookService::Parser

    attr_accessor :page_access_token, :verify_token, :options

    def initialize(service_data)
      service_data       = service_data || {}
      keys               = service_data['keys'] || {}
      @page_access_token = keys['PAGE_ACCESS_TOKEN']
      @verify_token      = keys['VERIFY_TOKEN']
    end

    def register_hook(params = {})
      mode      = params['hub.mode']
      token     = params['hub.verify_token']
      challenge = params['hub.challenge']
      return { code: 200, body: challenge } if mode === 'subscribe' && token === verify_token
      { code: 403, status: 'Forbiden' }
    end

    def incomming_fb_message(params={}, messages=[])
      fb_params = FacebookService::Client.simplify_message(params)
      return if fb_params['error']

      if first_message?(fb_params)
        send_welcome_message(fb_params['sender'], messages)
      else
        send_normal_message(fb_params)
      end
    end
  end
end
