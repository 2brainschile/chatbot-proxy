module StatisticsService
  module BasicStatistics
    def total_new_conversations
      @conversations.count
    end

    def total_new_messages
      @total_messages = {}
      @total_messages.default_proc = proc { 0 }

      @total_fallback_messages = {}
      @total_fallback_messages.default_proc = proc { 0 }

      @conversations.each do |conversation|
        conversation.last["messages"].each do |message|
          next unless message.last['createdAt']

          created_at = convert_milliseconds_to_date(message.last['createdAt'])
          month = month_lowercase(created_at.month)

          @total_messages[month] += 1

          if message.last.dig('message', 'intentName') == 'Default Fallback Intent'
            @total_fallback_messages[month] += 1 
          end
        end
      end
    end

    def percentage_effectiveness
      fallback_messages_count = amount_total(@total_fallback_messages)
      total_messages_count = amount_total(@total_messages)
      100 - ((fallback_messages_count * 100) / total_messages_count)
    end

    def build_basic_statistics
      total_new_messages
      {
        total_conversations: total_new_conversations,
        total_messages: @total_messages,
        total_fallback_messages: @total_fallback_messages,
        percentage_effectiveness: "#{percentage_effectiveness}%"
      }
    end
  end
end
