class StatisticsService::ChatbotStatistics
  include StatisticsService::Requests
  include StatisticsService::BasicStatistics
  include StatisticsService::BotStatistics
  include StatisticsService::Helpers

  attr_accessor :firebase_client, :bot_id, :conversations, :total_messages, :total_fallback_messages

  def initialize(bot_id)
    @bot_id = bot_id
    @firebase_client = FirebaseWrapper.new

    fetch_conversations
    filter_conversation_by_month(6, 'months')
  end
end
