module StatisticsService
  module Requests
    def fetch_conversations
      @conversations = @firebase_client.fetch_conversations(@bot_id).body
    end

    def filter_conversation_by_month(interval, type_interval)
      time_interval = eval("#{interval}.#{type_interval}.ago")

      @conversations.keep_if do |key, conversation|
        first_msg_date = convert_milliseconds_to_date(conversation['messages'].first[1]['createdAt'])

        first_msg_date >= time_interval
      end
    end
  end
end