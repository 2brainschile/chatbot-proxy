module StatisticsService
  module Helpers
    def convert_milliseconds_to_date(milliseconds)
      Time.at(milliseconds/1000)
    end

    def month_lowercase(month_number)
      Date::MONTHNAMES[month_number].downcase
    end

    def amount_total(hash_messages)
      hash_messages.map { |k, v| v }.sum
    end
  end
end