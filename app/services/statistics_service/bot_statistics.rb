module StatisticsService
  module BotStatistics
    def generate_top_intents
      @top_intents = {}

      @conversations.each do |conversation|
        conversation.last["messages"].each do |message|
          if message.last['sender'] == 'bot'
            intent_id = message.last.dig('message', 'intentId')
            intent_name = message.last.dig('message', 'intentName')

            if @top_intents[intent_id]
              @top_intents[intent_id][:count] += 1
            else
              @top_intents[intent_id] = { 
                name: intent_name,
                count: 1
              }
            end
          end
        end
      end
    end

    def build_bot_statistics
      generate_top_intents
      { intents: @top_intents }
    end
  end
end