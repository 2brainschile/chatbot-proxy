class MiddlewareService::Messages
  def analyze_bot_message(obj_bot_message, extra_params = nil, api_ai_client = nil)
    bot_message = obj_bot_message.dig(:body, :result, :fulfillment, :speech)
    action_message = obj_bot_message.dig(:body, :result, :action)

    reset_context(bot_message, api_ai_client)

    middleware_action = msg_belongs_to_middleware_action?(bot_message)
    middleware_action = actions_belongs_to_middleware_action?(action_message) unless middleware_action

    if middleware_action
      perform_action(middleware_action, obj_bot_message, extra_params)
    else
      obj_bot_message
    end
  end

  def reset_context(bot_message, api_ai_client = nil)
    if api_ai_client && ['CA - Necesito un email', 'CA - consulta'].include?(bot_message)
      api_ai_client.send_message('hola')
    end
  end

  def msg_belongs_to_middleware_action?(bot_message)
    messages_need_to_be_intervened.detect do |obj_message|
      return obj_message if obj_message[:message] == bot_message
    end
  end

  def messages_need_to_be_intervened
    [
      { message: 'CA - Necesito un email', service: 'CA', action: 'create' },
      { message: 'CA - consulta', service: 'CA', action: 'get' }
      
    ]
  end

  def actions_belongs_to_middleware_action?(action_message)
    actions_need_to_be_intervened.detect do |action|
      return action if action == action_message
    end
  end

  def actions_need_to_be_intervened
    [
      'can_doevluacion.can_doevluacion-custom'
    ]
  end

  def valid_json?(json)
    begin
      JSON.parse(json)
      return true
    rescue JSON::ParserError => e
      return false
    end
  end

  def perform_action(obj_message, obj_bot_message, extra_params)
    if obj_message == 'can_doevluacion.can_doevluacion-custom'
      nps_value = obj_bot_message.dig(:body, :result, :parameters, :npsValue)
      resp =  BotStatsService::Stats.new.save_value_to_nps('1c965db7ba3e4608b1ef253a643e4770', nps_value)
      return replace_message_bot(obj_bot_message, resp)
    end
    if obj_message[:service] == 'CA' && obj_message[:action] == 'get'
      ca_service = CaService::Client.new
      token = ca_service.get_token
      # email = extra_params['extra_params']['email']
      email = 'victor.bulnes@kcl.cl'
      # email ='carolina.araneda-brave@kcl.cl'
      # email ='fabian.arancibia@kcl.cl'
      begin
        resp = ca_service.get_ticket_by_email(token, email)

        if valid_json?(resp) && JSON.parse(resp)['card']['error']
          return replace_message_bot(obj_bot_message, 'No se encontraron tickets relacionados con tu email')
        else
          return replace_message_bot(obj_bot_message, resp, 'CA_SERVICE')
        end
      rescue
        return replace_message_bot(obj_bot_message, 'No se encontraron tickets relacionados con tu email')
      end
    end

    if obj_message[:service] == 'CA' && obj_message[:action] == 'create'
      ca_service = CaService::Client.new
      token = ca_service.get_token
      summary = obj_bot_message.dig(:body, :result, :parameters, :ticketSummary)
      description = obj_bot_message.dig(:body, :result, :parameters, :ticketDescription)
      email = extra_params['extra_params']['email']

      begin
        resp = ca_service.create_ticket(token, summary, description, email)
        return replace_message_bot(obj_bot_message, resp)
      rescue
        return replace_message_bot(obj_bot_message, 'Ocurrio un error con el servicio de tickets')
      end
    end
  end

  def replace_message_bot(obj_bot_message, new_speech, type = 0)
    obj_bot_message[:body][:result][:fulfillment][:speech] = new_speech
    obj_bot_message[:body][:result][:fulfillment][:messages] = [{:type=>type, :speech=> new_speech}]
    obj_bot_message
  end
end


