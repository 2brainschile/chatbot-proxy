class FirebaseWrapper
  attr_accessor :firebase_client
  TIME_DIFF_SECS = 900 # 900 -> 15 minutes

  def initialize
    @firebase_client = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
  end

  # TODO: Check if this method is being used.
  def push_chatroom(chatboot_id, data = {})
    check_params(chatboot_id: chatboot_id)
    resp = firebase_client.push("chats/#{chatboot_id}", build_options_to_chatroom(data))
    return resp
  end

  def set_chatroom(chatboot_id, sender_id, data = {})
    # Check if chatroom exists before creating it
    existing_chatroom = firebase_client.get("chatrooms/#{chatboot_id}/#{sender_id}")
    if existing_chatroom.body
      existing_chatroom
    else
      create_chatroom(chatboot_id, sender_id, data)
    end
  end

  def create_chatroom(chatbot_id, sender_id, data = {})
    check_params(chatbot_id: chatbot_id, sender_id: sender_id)
    opts = build_options_to_chatroom(data.merge({sender_id: sender_id}))
    firebase_client.set("chatrooms/#{chatbot_id}/#{sender_id}", opts)
  end

  def chatroom_exists?(chatbot_id, sender_id)
    resp = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}")
    resp.body # nil or chatroom body
  end

  def fix_chatroom_data(chatbot_id, sender_id, data)
    check_params(chatbot_id: chatbot_id, sender_id: sender_id)
    opts = build_options_to_chatroom(data.merge({sender_id: sender_id}))
    firebase_client.update("chatrooms/#{chatbot_id}/#{sender_id}", opts)
  end

  def add_message(chatboot_id, sender_id, message = {})
    check_params(chatboot_id: chatboot_id, sender_id: sender_id)
    resp = firebase_client.push("chats/#{chatboot_id}/#{sender_id}/messages",
                                { createdAt: Firebase::ServerValue::TIMESTAMP,
                                  message: message[:body],
                                  sender: message[:sender],
                                  ignore: message[:ignore] })
    update_chatroom_data(chatboot_id, sender_id, message)
    resp
  end

  def add_broadcast_message(chatboot_id, recipents = [], message = {})
    check_params(chatboot_id: chatboot_id)
    firebase_client.push("broadcasted_messages/#{chatboot_id}",
                                { createdAt: Firebase::ServerValue::TIMESTAMP,
                                  recipients: recipents,
                                  message: message[:body] })
  end

  def add_notification_message(chatboot_id, notification)
    check_params(chatboot_id: chatboot_id)
    firebase_client.push("notifications/#{chatboot_id}", 
                      notification.merge({ createdAt: Firebase::ServerValue::TIMESTAMP}))
  end

  def add_whatsapp_notifier(chatboot_id, notification)
    check_params(chatboot_id: chatboot_id)
    firebase_client.push("notifiers/#{chatboot_id}", 
                      notification.merge({ createdAt: Firebase::ServerValue::TIMESTAMP}))
  end

  # Checks if chat became active

  def manual_mode?(chatbot_id, sender_id)
    resp = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}/manualMode/active")
    manual_mode = resp.body
  end

  def should_notify?(chatbot_id, sender_id)
    last_message = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}/lastMessage").body
    return false if last_message.nil?
    time_in_secs = last_message['createdAt'] / 1000
    Time.now.to_i - time_in_secs > TIME_DIFF_SECS
  end

  def build_contact_info(chatbot_id, sender_id)
    contact_info = {}
    user_name  = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}/userName").body
    user_email = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}/userEmail").body
    user_phone = firebase_client.get("chatrooms/#{chatbot_id}/#{sender_id}/userPhone").body
    contact_info[:user_name] = user_name if user_name
    contact_info[:user_email] = user_email if user_email
    contact_info[:user_phone] = user_phone if user_phone
    contact_info
  end

  # TODO: Check if this method fails or not
  def update_chatroom_data(chatbot_id, sender_id, message = {})
    now = Firebase::ServerValue::TIMESTAMP
    data = { lastMessageAt: now, lastMessage: { createdAt: now,
                            message: message[:body] } }
    resp = firebase_client.update("chatrooms/#{chatbot_id}/#{sender_id}", data)
  end

  def fetch_bot_name(app_id)
    firebase_client.get("search_project_id/#{app_id}").body
  end

  def fetch_services(app_id)
    response = firebase_client.get("search_project_id/#{app_id}").body
    project_id = response || 'ERROR'
    firebase_client.get("projects/#{project_id}/services").body
  end

  def build_options_to_chatroom(data)
    opts = build_basic_options(data)
    opts = add_webapp_options(opts, data) if data.has_key?(:raw_user_data)
    opts = opts.merge({ userPhone: data[:user_phone] }) if data[:user_phone]
    opts = opts.merge({ userRut: data[:user_rut] }) if data[:user_rut]
    opts = opts.merge({ userImage: data[:user_image] }) if data[:user_phone]
    opts
  end

  def build_basic_options(data)
    { createdAt: Firebase::ServerValue::TIMESTAMP,
      userName:  data[:user_name],
      parseDate: DateTime.now.to_s,
      appId:     data[:app_id],
      userId:    data[:sender_id], # ?
      origin:    data[:origin] }
  end

  def add_webapp_options(basic_data, full_data)
    raw_user_data = full_data[:raw_user_data]
    final_data = basic_data.merge({ userEmail: raw_user_data[:user_email],
                                    userRut: raw_user_data[:user_rut],
                                    userPhone: raw_user_data[:user_phone] })
    final_data
  end

  def create_default_intents(intent)
    intent = intent.to_h
    firebase_client.push("settings/default_intents/", intent)
  end

  def get_all_default_intent
    firebase_client.get("settings/default_intents/")
  end

  def get_default_intent(id)
    firebase_client.get("settings/default_intents/#{id}")
  end

  def update_default_intent(intent)
    intent = intent.to_h
    id = intent['id']
    intent.delete("id")
    firebase_client.update("settings/default_intents/#{id}", intent)
  end

  def delete_default_intent(id)
    firebase_client.delete("settings/default_intents/#{id}")
  end

  def fetch_conversations(bot_id)
    firebase_client.get("chats/#{bot_id}")
  end

  def check_params(params = {})
    params.each do |key, value|
      raise ArgumentError.new("Everyone must have a #{key.to_s}") if value == nil
    end
  end

  def search_bot_by_google_hangouts_id(hangouts_id)
    firebase_client.get("google_hangouts_ids/#{hangouts_id}")
  end

  def save_stats(bot_id, path, stats)
    firebase_client.push("stats/#{bot_id}/#{path}", stats)
  end

  def update_stats(bot_id, stats)
    firebase_client.update("stats/#{bot_id}", stats)
  end

  def get_stats(bot_id, path)
    firebase_client.get("stats/#{bot_id}/#{path}")
  end
end
