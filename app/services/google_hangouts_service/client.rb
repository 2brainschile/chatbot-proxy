module GoogleHangoutsService
  class Client
    def search_api_ai_key(group_email)
      firebase_client = FirebaseWrapper.new
      resp = firebase_client.search_bot_by_google_hangouts_id(group_email)
      resp.body['API_AI']
    end

    def api_ai_reponse(google_hangouts_id, message, email)
      group_email = email.split('@').last.split('.').first
      bot_id = search_api_ai_key(group_email)
      keys = { keys: { 'API_AI_ID': bot_id} }
      extra_options = { api_session_id: email }
      api_ai = ApiaiService::Client.new(keys, extra_options)
      bot_response = api_ai.send_message(message)

      msg = MiddlewareService::Messages.new.analyze_bot_message(bot_response, { 'extra_params' => { 'email' => email }}, api_ai)

      fulfillment = msg.dig(:body, :result, :fulfillment)
      # main_msg = fulfillment.dig(:speech)
      # card_msg = fulfillment.dig(:messages).detect { |msg| msg[:type] == 4 }

      # if card_msg
      #   build_hangouts_card(card_msg)
      # else
      #   main_msg
      # end

      fulfillment.dig(:messages)
    end

    def detect_CA_msg(messages)
      messages.detect { |msg| msg[:type] == 'CA_SERVICE' }
    end

    def detect_card_msg(messages)
      messages.detect { |msg| msg[:type] == 4 }
    end

    def valid_json?(json)
      begin
        JSON.parse(json)
        return true
      rescue JSON::ParserError => e
        return false
      end
    end

    def response(google_hangouts_id, message, email)
      response = api_ai_reponse(google_hangouts_id, message, email)

      is_card_CA = detect_CA_msg(response)
      is_card = detect_card_msg(response)

      if is_card_CA
        card = JSON.parse(is_card_CA[:speech])['card']
        build_card_to_tickets(card)
      elsif response.first[:speech] && valid_json?(response.first[:speech])
        card = JSON.parse(response.first[:speech])['card']
        build_CA_card_message(card)
      elsif is_card
        # binding.pry
        build_card_message(response)
      else
        response = response.inject('') do |str, msg|
          str += "#{msg[:speech]}\n"
        end
        { text: response }
      end
    end

    def build_card_message(messages)
      {
        "cards": [
          {
            "sections": [
              {
                "widgets": build_widgets(messages)
              }
            ]
          }
        ]
      }
    end

    def build_widgets(messages)
      messages.inject([]) do |rtn, msg|
        if msg[:type] == 0
          rtn << build_simple_msg_to_card(msg[:speech])
        elsif msg[:type] == 4 && msg[:platform] != 'facebook'
          title = msg[:payload][:custom_message][:text]
          buttons = msg[:payload][:botones]
          rtn << {
            "textParagraph": {
              "text": title
            }
          }
          if buttons
            rtn << {
              "buttons": build_buttons(buttons)
            }
          end
        end
      end
    end

    def build_simple_msg_to_card(text)
      {
        "textParagraph": {
          "text": "#{text}"
        }
      }
    end

    def build_CA_card_message(card)
      {
        "cards": [
          {
            "header": {
              "title": "Información del ticket",
              "subtitle": "#{card['body']['title']}",
              "imageUrl": "https://cdn4.iconfinder.com/data/icons/48-bubbles/48/12.File-512.png"
            },
            "sections": [
              {
                "widgets": [
                  {
                    "keyValue": {
                      "topLabel": "Descripción",
                      "content": "#{card['body']['description']}"
                      }
                  },
                  {
                    "keyValue": {
                      "topLabel": "Estado",
                      "content": "#{card['body']['specialList'].first['content']}"
                    }
                  },
                  {
                    "keyValue": {
                      "topLabel": "Asignado a:",
                      "content": "#{card['body']['specialList'].last['content']}"
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    end

    def build_hangouts_card(msg)
      title = msg.dig(:payload, :custom_message, :text)
      buttons =  msg.dig(:payload, :botones)
      {
        "actionResponse":{
          "type": "NEW_MESSAGE"
        },
        "cards": [
          {
            "sections": [
              {
                "widgets": [
                  {
                    "textParagraph": {
                      "text": title
                    }
                  },
                  {
                    "buttons": build_buttons(buttons)
                  }
                ]
              }
            ]
          }
        ]
      }
    end

    def build_buttons(buttons)
      return [] unless buttons
      buttons.inject([]) do |rtn, btn|
        rtn <<  {
            "textButton": {
              "text": btn[:label],
              "onClick": {
                "action": {
                  "actionMethodName": "OPTION_#{btn[:boton]}",
                  "parameters": [
                    {
                      "key": "text",
                      "value": btn[:label]
                    }
                  ]
                }
              }
            }
          }
      end
    end

    def build_card_to_tickets(msg)
      tickets = msg["body"]["normalList"]["items"]
      {
        "cards": [
          {
            "header": {
              "title": "Información de tus tickets",
            },
            "sections": [
              {
                "widgets": build_tickets_msg(tickets)
              }
            ]
          }
        ]
      }
    end

    def build_tickets_msg(tickets)
      return [] unless tickets
      tickets.inject([]) do |rtn, ticket|
        rtn <<  {
          "textParagraph": {
            "text": "<b>ID:</b> #{ticket.first}, <b>Estado:</b> #{ticket.last}"
          }
        }
      end
    end
  end
end
