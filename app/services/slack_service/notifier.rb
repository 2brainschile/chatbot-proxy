# Service wrapper for the Slack client
require 'slack_service/helpers'
require 'slack_service/notifications'

module SlackService
  class Notifier
    include SlackService::Helpers

    def initialize(service_data, bot_name)
      @client = Client.new(service_data, bot_name)
      @notification_handlers = { Notifications::NEW_MESSAGE  => :send_new_message_notification,
                                 Notifications::NEW_CHATROOM => :send_new_message_notification,
                                 Notifications::LOW_ACCURACY => :send_low_accuracy_notification,
                                 Notifications::RESTARTED    => :send_new_message_notification }
      @notifications = []
    end

    def has_notifications?
      !@notifications.empty?
    end

    def notify(params)
      @notifications.each do |notification|
        method = @notification_handlers[notification[:type]]
        send(method, params)
      end
    end

    def add(notification)
      # notification = { type: , data: ? }
      return if notification.nil?
      @notifications.push(notification)
    end

    def send_slack_notification(message, params)
      chat_id    = params[:sender]
      chatbot_id = params[:chatbot_id]
      link = "https://smartbots.cl/contact-center/index.html#/messages/" +
      "#{chatbot_id}/#{chat_id}"
      message += " <#{link}|Ingresar a la Conversación>"
      
      field_data = { field_title: 'Contact info' }
      field_data[:fields] = params[:contact_info] || {}
      field_data[:fields] = field_data[:fields].merge(params[:extra_data])
      resp = @client.create_new_message(message, field_data)
    end
    
    def send_new_message_notification(params)
      message = "Alguien comenzó a hablar con el bot de *#{params[:chatbot_name].upcase}*"
      send_slack_notification(message, params)
    end
    
    def send_low_accuracy_notification(params)
      chatbot_name = params[:chatbot_name]
      message = "Se ha detectado baja exactitud en una conversacion en *#{chatbot_name.upcase}*"
      send_slack_notification(message, params)
    end
  end
end
