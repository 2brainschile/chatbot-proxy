# Service wrapper for the Slack client
module SlackService
    module Helpers
      # Builds complex message with attachments
      # field_data is expected to have
      # field_title: ""
      # fields: {}
      def build_full_attachments(field_data = {})
        fields = build_attachment_fields(field_data[:fields] || {})
        attachments = [
          {
            "fallback": "Mensaje de #{bot_name}",
            "color": "#36a64f",
            "author_name": @bot_name,
            "author_link": "http://www.smartbots.cl",
            "author_icon": "http://i.imgur.com/soDmHlu.png",
            "title": field_data[:fields_title],
            "fields": fields,
            "footer": @bot_name,
            "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
            "ts": DateTime.now.to_i
          }
        ]
        attachments
      end

      def build_attachment_fields(data)
        final_fields = []
        data.each do |k, v|
          field = build_field(k.to_s.capitalize, v, true)
          final_fields.push(field)
        end
        final_fields
      end

      def build_field(title, value, short)
        { title: title, value: value, short: short } 
      end
    end
  end
  