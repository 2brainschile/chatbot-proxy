# Service wrapper for the Slack client
require 'slack_service/helpers'

module SlackService
  class Client
    include SlackService::Helpers

    attr_reader :slack_client
    attr_reader :bot_name

    # TODO: Initialize with bot name (not ID)
    def initialize(service_data, bot_name)
      service_data = service_data || {}
      keys         = service_data['keys'] || {}
      @recipents   = service_data['recipents'] || {}
      @bot_name    = bot_name
      @client      = Slack::Web::Client.new(token: keys['TOKEN'])
    end

    # Allows other services to send messages through Slack
    def create_new_message(notif_message, field_data)
      attachments = build_full_attachments(field_data)
      message_hash = { text: notif_message, attachments: attachments, as_user: true }
      success = send_message_to_multiple_recipents(message_hash, @recipents)
      success
    end

    def send_message_to_multiple_recipents(message_hash, recipents)
      success = true
      recipents.each do |channel, _|
        begin
          final_hash = message_hash.merge({ channel: channel })
          @client.chat_postMessage(final_hash)
        rescue
          # Set success to false if at least one messages fails to send
          success = false
        end
      end
      success
    end
  end
end
