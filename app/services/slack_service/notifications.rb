# Service wrapper for the Slack client
module SlackService
  module Notifications
    NEW_MESSAGE  = 'New message'
    NEW_CHATROOM = 'New chatroom'
    LOW_ACCURACY = 'Low accuracy or fallback'
    RESTARTED    = 'Conversation restarted'
  end
end
  