module BotStatsService
  class Stats
    def save_value_to_nps(bot_id, value)
      save_answers(bot_id, value)
      "Gracias por contestar, su repuesta ha sido guardada"
    end

    def save_answers(bot_id, value)
      firebase_client = FirebaseWrapper.new
      firebase_client.save_stats(bot_id, 'answers_nps', {value: value})
    end

    def search_answers(bot_id)
      firebase_client = FirebaseWrapper.new
      answers = firebase_client.get_stats(bot_id, 'answers_nps').body
      # binding.pry
      result = calculate_nps(answers)
      save_nps(bot_id, result)
    end

    def save_nps(bot_id, result)
      firebase_client = FirebaseWrapper.new
      firebase_client.update_stats(bot_id, {total_nps: result})
    end

    def calculate_nps(answers)
      total_answers = answers.count
      promotores = 0
      # pasivos = 0
      detractores = 0

      answers.each do |key, value|
        val = value["value"].to_i
        promotores += 1 if val == 9 || val == 10
        # pasivos += 1 if val == 7 || val == 8
        detractores += 1 if val <= 6
      end

      porcentaje_promotores = (promotores * 100) / total_answers
      # porcentaje_pasivos = (pasivos * 100) / total_answers
      porcentaje_detractores = (detractores * 100) / total_answers

      porcentaje_promotores - porcentaje_detractores
    end
  end
end
