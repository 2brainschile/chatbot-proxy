module HandlerService
  module FirebaseUtils

    def post_user_message_to_firebase(handle_params)
      user_msg = handle_params['message']['text']
      user_id  = handle_params['sender']
      ignore   = false
      add_messages_to_firebase(user_id, [user_msg], 'customer', ignore)
    end

    def post_bot_message_to_firebase(handle_params)
      user_id  = handle_params['sender']
      ignore   = handle_params['ignore']
      score = @bot_response[:body][:result][:score]
      is_fallback = @bot_response[:body][:result][:metadata][:intentName] == 'Default Fallback Intent'

      if (score < 0.5 || is_fallback) && @notify
        user_msg = handle_params['message'] ? handle_params['message']['text'] : 'SIN MENSAJE'
        add_extra_data_to_notif(user_message: user_msg)
        @slack_notifier.add(type: SlackService::Notifications::LOW_ACCURACY)
      end

      messages = api_ai_client.extract_messages(@bot_response) || []

      score = @bot_response[:body][:result][:score] || 0
      intent_id = @bot_response[:body][:result][:metadata][:intentId] || 'NN'
      intent = @bot_response[:body][:result][:metadata][:intentName] || 'NN'
      type_message = @bot_response[:body][:result][:parameters][:type_message] || 'normal'

      options = {
        user_id: user_id,
        messages: messages,
        sender_type: 'bot',
        ignore: ignore,
        score: score,
        intent_id: intent_id,
        intent_name: intent,
        type_message: type_message
      }
      add_bot_messages_to_firebase(options)
    end

    def post_manual_message_to_firebase(handle_params)
      messages = handle_params[:messages]
      user_id  = handle_params[:user_id]
      ignore   = handle_params['ignore']
      add_messages_to_firebase(user_id, messages, 'contact-center', ignore)
    end

    def add_to_whatsapp_firebase_notifier(handle_params)
      firebase_client.add_whatsapp_notifier(handle_params[:chatbot_id], handle_params)
    end

    def create_chatroom_in_firebase(user_name, app_id, origin, user_id = nil)
      data = { user_name: user_name, app_id: app_id, origin: origin }
      data[:raw_user_data] = @raw_user_data if @raw_user_data

      if user_id
        # TODO: instead of SET we should check for chatroom existence,
        # then decide if we should set or UPDATE
        resp = firebase_client.set_chatroom(api_ai_key, user_id, data)
      else
        resp = firebase_client.push_chatroom(api_ai_key, data)
      end

      if resp.success?
        { body: resp.body["name"], status: 200 }
      else
        { body: resp, status: resp.code }
      end
    end

    # Checks if chatroom doesnt exist or if it has missing data.
    # For facebook
    def check_and_fix_chatroom(handle_params)
      sender_id = handle_params['sender']
      chatroom = firebase_client.chatroom_exists?(@api_ai_key, sender_id)

      if chatroom
        if !chatroom.key?('userName')
          data = build_facebook_user_data(handle_params)
          firebase_client.fix_chatroom_data(@api_ai_key, sender_id, data)
        end
      else
        data = build_facebook_user_data(handle_params)
        firebase_client.create_chatroom(@api_ai_key, sender_id, data)
      end
    end

    # For whatsapp
    def check_or_create_chatroom(handle_params)
      sender_id = handle_params['sender']
      chatroom = firebase_client.chatroom_exists?(@api_ai_key, sender_id)
      if !chatroom
        data = { user_name:  handle_params['contact_info'][:user_name],
                 user_phone: handle_params['contact_info'][:user_phone],
                 user_image: handle_params['contact_info'][:user_image],
                 origin:     'whatsapp' }
        client_resp = firebase_client.create_chatroom(@api_ai_key, sender_id, data)
        @slack_notifier.add(type: SlackService::Notifications::NEW_CHATROOM)
        resp = { body: client_resp.body, status: client_resp.code }
      else
        resp = { body: '', status: 200 }
      end
      resp
    end

    def build_facebook_user_data(handle_params)
      user_name = facebook_client.user_name(handle_params['sender'])
      app_id    = handle_params['fanpage_id']
      data = { user_name: user_name, app_id: app_id, origin: 'facebook' }
    end

    def add_messages_to_firebase(user_id, messages, sender_type, ignore)
      notify = firebase_client.should_notify?(api_ai_key, user_id)
      @slack_notifier.add(type: SlackService::Notifications::RESTARTED) if notify
      response = {}
      messages.each do |msg|
        options = { body: msg, sender: sender_type, ignore: ignore }
        response = firebase_client.add_message(api_ai_key, user_id, options)
        return { body: response, status: response.code } unless response.success?
      end
      { body: response, status: 200 }
    end

    def add_bot_messages_to_firebase(options = {})
      notify = firebase_client.should_notify?(api_ai_key, options[:user_id])
      @slack_notifier.add(type: SlackService::Notifications::RESTARTED) if notify
      response = {}
      messages = message_parser(options[:messages])
      options[:messages].each do |msg|
        msg[:intentName] = options[:intent_name]
        msg[:score] = options[:score]
        msg[:intentId] = options[:intent_id]
        msg[:type_message] = options[:type_message]
        opts = {
          body: msg,
          sender: options[:sender_type],
          ignore: options[:ignore]
        }
        response = firebase_client.add_message(api_ai_key, options[:user_id], opts)
        return { body: response, status: response.code } unless response.success?
      end
      { body: response, status: 200 }
    end

    def message_parser(response_result)
      messages = []
      filtered_for_facebook_platform(response_result).each do |msg|
        messages << format_based_on_type(msg.deep_symbolize_keys)
      end
      messages
    end

    def format_based_on_type(msg)
      case msg[:type]
       when 3
        {
          'attachment': {
            'type': 'image',
            'payload': {
              'url': msg[:imageUrl]
            }
          }
        }
      when 4
        msg[:payload][:facebook]
      when 0
        msg[:speech]
      else
        msg[:speech]
      end
    end

    def filtered_for_facebook_platform(response_result)
      if has_platform_key?(response_result)
        response_result.keep_if { |hash| hash[:platform] == "facebook" }
      else
        response_result
      end
    end

    def has_platform_key?(response_result)
      counter = 0
      if response_result
        response_result.each do |hash|
          if hash.keys.include?(:platform) then counter += 1 end
        end
      end
      counter > 0 ? true : false
    end
  end
end
