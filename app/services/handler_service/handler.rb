# This Service is responsible for calling external services
# At this time it calls APIAI, FACEBOOK MESSENGER and FIREBASE itergrations
require 'facebook_service/client'
require 'apiai_service/client'
require 'slack_service/client'
require 'slack_service/notifications'
require 'handler_service/response_status'
require 'handler_service/firebase_utils'
require 'handler_service/api_ai_utils'
require 'handler_service/facebook_utils'
require 'handler_service/handler_utils'
require 'handler_service/whatsapp_utils'

require 'base64'

module HandlerService
  class Handler
    include HandlerService::ResponseStatus
    include HandlerService::FirebaseUtils
    include HandlerService::ApiAIUtils
    include HandlerService::FacebookUtils
    include HandlerService::HandlerUtils
    include HandlerService::WhatsappUtils

    attr_reader :api_ai_key,
                :firebase_client,
                :api_ai_client,
                :facebook_client,
                :bot_name

    def initialize(app_id, api_ai_opts = {}, notify = true)
      # TODO: Check which services should be initialized
      # Considerar mover la asignación de los clientes a los métodos que usan
      # los servicios. Ejemplo, no todos usan el cliente facebook
      @firebase_client = FirebaseWrapper.new
      @bot_name = firebase_client.fetch_bot_name(app_id)
      services = firebase_client.fetch_services(app_id) || {}
      @notify = notify
      @slack_notifier  = SlackService::Notifier.new(services['slack'], @bot_name)
      @facebook_client = FacebookService::Client.new(services['facebook'])
      @api_ai_client   = ApiaiService::Client.new(services['api_ai'], api_ai_opts)
      @api_ai_key = @api_ai_client.key
    end

    # Registers proxy with facebook
    def register_facebook_hook(hook_params)
      resp = facebook_client.register_hook(hook_params)
      define_response_message(resp)
    end

    def handle_new_facebook_message(message_params)
      response = {}
      message = facebook_client.simplify_message(message_params)
      build_notification_data(message['sender'])
      if facebook_client.first_message?(message)
        response = send_welcome_message_to_facebook(message)
      elsif message['read']
        # TODO: Append seen action to current message
        #response = send_seen_message_to_facebook(message)
        response = message_success(body: 'No message sent', status: 200)
      else
        message_text = message['message']['text']
        # TODO: Check if we should support answers for messages with no text.
        if message_text.nil? || message_text.length < 1
          response = message_success(body: 'No message sent', status: 200)
        else
          check_and_fix_chatroom(message)
          response = send_normal_message_to_facebook(message)
        end
      end
      send_notifications
      response_for_facebook_webhook(response)
    end

    # Comes from the contact center
    def handle_new_manual_message(params = {})
      facebook_resp = forward_message_to_facebook(params)
      firebase_resp = post_manual_message_to_firebase(params)
      { facebook: facebook_resp, firebase: firebase_resp }
    end

    def handle_new_web_app_chatroom(params)
      @raw_user_data = params
      safe_params = web_app_params(params)
      build_notification_data(safe_params[:sender], @raw_user_data.to_h)

      response = send_welcome_message_to_web_app(safe_params)
      send_notifications
      { body: safe_params[:user_email], status: 200 }
    end

    def handle_new_chrome_ext_chatroom(params)
      # TODO: Check if chatroom already exists before creating
      @raw_user_data = params
      safe_params = chrome_ext_params(params)

      response = send_welcome_message_to_chrome_ext(safe_params)
      { body: safe_params[:user_email], status: response[:body].code }
    end

    def handle_new_web_app_message(params)
      handle_params = { 'sender'  => params['chatroom_id'],
                        'ignore'  => params['ignore'],
                        'message' => { 'text' => params['message'] }
                      }

      merge_extra_params!(handle_params, params)

      contact_info = @firebase_client.build_contact_info(@api_ai_key,
                                                         handle_params['sender'])

      build_notification_data(handle_params['sender'], contact_info)
      response = send_normal_message_to_web_app(handle_params)
      send_notifications
      response
    end

    def handle_new_chrome_ext_message(params)
      handle_params = { 'sender'  => params['chatroom_id'],
                        'ignore'  => params['ignore'],
                        'message' => { 'text' => params['message'] } }

      contact_info = @firebase_client.build_contact_info(@api_ai_key,
                                                         handle_params['sender'])
      response = send_normal_message_to_web_app(handle_params)
      response
    end

    def handle_new_whatsapp_message(params)
      # TODO: Get params correctly
      contact_info = { user_name: params[:user], user_phone: params[:phone], user_image: params[:img] }
      sender_id = params[:user_id].gsub('.', ',')
      handle_params = { 'sender'       => sender_id,
                        'contact_info' => contact_info,
                        'ignore'       => false,
                        'message'      => { 'text' => params[:message] } }

      build_notification_data(handle_params['sender'], contact_info)
      response = send_normal_message_to_whatsapp(handle_params)
      send_notifications
      response
    end

    def handle_new_manual_message_in_whatsapp(params)
      firebase_resp = post_manual_message_to_firebase(params.merge({ user_id: params[:user_id].gsub('.', ','), messages: [params[:message]] }))
      notifier_resp = add_to_whatsapp_firebase_notifier(params)
      { body: {db: firebase_resp, notifier: notifier_resp }, status: 200}
    end

    def web_app_params(params)
      # user_email gets base64 encoded to avoid issues with firebase
      encoded_email = Base64.strict_encode64(params[:user_email]).gsub('=', '')
      new_params = { chatbot_id: params[:chatbot_id],
                     user_email: encoded_email,
                     user_name: params[:user_name],
                     sender: encoded_email }

      ActionController::Parameters.new(new_params)
    end

    def chrome_ext_params(params)
      encoded_email = Base64.strict_encode64(params[:user_email]).gsub('=', '')
      new_params = { chatbot_id: params[:chatbot_id],
                     user_email: params[:user_email],
                     user_name: params[:user_name],
                     sender: encoded_email }

      ActionController::Parameters.new(new_params)
    end

    def send_notifications
      @slack_notifier.notify(@notif_data) if @slack_notifier.has_notifications?
    end

    def build_notification_data(sender, contact_info = {})
      basic_data = { chatbot_id: @api_ai_key,
                     sender: sender,
                     chatbot_name: @bot_name }
      basic_data[:extra_data] = {}
      basic_data[:contact_info] = contact_info unless contact_info.empty?
      @notif_data = basic_data
    end

    def add_extra_data_to_notif(entry = {})
      @notif_data[:extra_data] = @notif_data[:extra_data].merge(entry)
    end

    # Facebook will retry every message that doesnt return status = 200
    def response_for_facebook_webhook(response)
      status = response[:status]
      resp_body = response[:body]
      body   = resp_body.respond_to?('body') ? resp_body.body : resp_body
      if status != 200
        begin
          raise Exception.new("Error #{status} on facebook webhook. Body: #{body}")
        rescue Exception => exception
          Raven.capture_exception(exception)
        end
      end
      response[:status] = 200
      response
    end

    def broadcast_message(broadcast_params)
      puts 'broadcasting message..'
      chatbot_id = broadcast_params[:chatbot_id]
      message = broadcast_params[:message]
      recipents = broadcast_params[:recipents].map { |recipent_params| recipent_params.to_h }
      BroadcastMessageJob.perform_later(chatbot_id, message, recipents)
    end

    def generate_report(report_params)
      chatbot_id = broadcast_params[:chatbot_id]
      ReportCsvJob.perform_later(chatbot_id)
    end

    def notification_message notification_params
      chatbot_id = notification_params[:chatbot_id]
      message_obj = {
        title: notification_params[:title] || "Nueva notificación",
        content: notification_params[:message],
        image_url: notification_params[:image_url],
        link: notification_params[:link],
      }
      @firebase_client.add_notification_message(chatbot_id, message_obj)
    end

    def merge_extra_params!(handle_params, params)
      if params.dig('extra_params', 'email')
        handle_params.merge!(
          {'extra_params' => { 'email' => params['extra_params']['email'] }}
        )
      end
    end
  end
end
