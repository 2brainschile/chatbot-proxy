# Main module for handling and dispatching methods calls throw different services
# in order to acomplish a defined message. See :send_welcome_message_to_facebook
module HandlerService
  module HandlerUtils
    def call_methods(methods_array, params_to_methods)
      response = {}
      methods_array.each do |method|
        response = send(method, params_to_methods)
        return response unless response[:status] == 200
      end
      message_success(response)
    end

    def send_welcome_message_to_facebook(handle_params)
      steps = %i[ create_chatroom_for_facebook
                  welcome_response_for_facebook
                  post_bot_message_to_firebase
                  post_bot_message_to_facebook ]

      @slack_notifier.add(type: SlackService::Notifications::NEW_MESSAGE)
      call_methods(steps, handle_params)
    end

    def send_welcome_message_to_web_app(handle_params)
      steps = %i[ create_chatroom_for_web_app
                  welcome_response_for_web_app
                  post_bot_message_to_firebase ]

      @slack_notifier.add(type: SlackService::Notifications::NEW_CHATROOM)
      call_methods(steps, handle_params)
    end

    def send_welcome_message_to_chrome_ext(handle_params)
      steps = %i[ create_chatroom_for_chrome_ext
                  welcome_response_for_web_app
                  post_bot_message_to_firebase ]

      call_methods(steps, handle_params)
    end

    def send_normal_message_to_facebook(handle_params)
      handle_params = inject_ignore_param(handle_params)
      steps = %i[ post_user_message_to_firebase
                  fetch_messages_from_api_ai
                  post_bot_message_to_firebase
                  post_bot_message_to_facebook ]
      call_methods(steps, handle_params)
    end

    def send_normal_message_to_web_app(handle_params)
      steps = %i[ post_user_message_to_firebase
                  fetch_messages_from_api_ai
                  post_bot_message_to_firebase ]
      call_methods(steps, handle_params)
    end

    def send_normal_message_to_whatsapp(handle_params)
      # Check if chatroom exists, if not, create it.
      handle_params = inject_ignore_param(handle_params)
      steps = %i[ check_or_create_chatroom
                  post_user_message_to_firebase
                  fetch_messages_from_api_ai
                  post_bot_message_to_firebase
                  format_bot_response ]
      # Filter bot response to exclude attachments
      call_methods(steps, handle_params)
    end

    def create_chatroom_for_web_app(params)
      user_name  = params[:user_name]
      user_email = params[:user_email]
      # TODO: What should the app_id be for web_app ?
      create_chatroom_in_firebase(user_name, nil, 'web_app', user_email)
    end

    def create_chatroom_for_chrome_ext(params)
      # Calling create_chatroom_in_firebase checks existance before creating
      user_name  = params[:user_name]
      user_email = params[:user_email]
      user_id = params[:sender]
      create_chatroom_in_firebase(user_name, nil, 'chrome_ext', user_id)
    end

    def create_chatroom_for_facebook(handle_params)
      fanpage_id   = handle_params['fanpage_id']
      fb_user_id   = handle_params['sender']
      fb_user_name = facebook_client.user_name(fb_user_id)
      create_chatroom_in_firebase(fb_user_name, fanpage_id, 'facebook', fb_user_id)
    end

    def welcome_response_for_web_app(params)
      user_name = params[:user_name]
      @bot_response = welcome_message_from_api_ai(user_name)
    end

    def welcome_response_for_facebook(handle_params)
      fb_user_id    = handle_params['sender']
      fb_user_name  = facebook_client.user_name(fb_user_id)
      @bot_response = welcome_message_from_api_ai(fb_user_name)
    end

    # Sends manual message from contact center to facebook
    def forward_message_to_facebook(params = {})
      resp = {}
      params['messages'].each do |message|
        options   = [params[:fanpage_id], params[:user_id], message]
        fb_params = facebook_client.build_contact_center_msg_obj(*options)
        resp = facebook_client.send_direct_message(fb_params)
      end
      { body: resp.body, status: resp.code }
    end

    def web_app_field_data
      return nil unless @raw_user_data
      data = { name:  @raw_user_data[:user_name],
               email: @raw_user_data[:user_email],
               phone: @raw_user_data[:user_phone] }

      { field_title: 'Datos de contacto', fields: data }
    end

    def inject_ignore_param(handle_params)
      fb_user_id = handle_params['sender']
      manual_mode = firebase_client.manual_mode?(@api_ai_key, fb_user_id)
      handle_params['ignore'] = manual_mode
      handle_params
    end
  end
end
