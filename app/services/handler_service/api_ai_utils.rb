module HandlerService
  module ApiAIUtils
    def fetch_messages_from_api_ai(handle_params)
      user_msg      = handle_params['message']['text']
      @bot_response = @api_ai_client.send_message(user_msg)
      MiddlewareService::Messages.new.analyze_bot_message(@bot_response, handle_params, @api_ai_client)
    end

    def welcome_message_from_api_ai(user_name)
      @api_ai_client.default_welcome('WELCOME', user_name)
    end
  end
end
