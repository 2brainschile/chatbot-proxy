module HandlerService
  module FacebookUtils
    def post_bot_message_to_facebook(handle_params)
      return { body: '', status: 200 } if handle_params['ignore']
      fb_user_id = handle_params['sender']
      messages   = facebook_client.message_parser(@bot_response[:body])
      messages   = replace_tags(messages)
      facebook_client.send_mutiple_messages(fb_user_id, messages)
    end

    def replace_tags(messages)
      output_messages = messages.map do |msg|
        # A message can be a string or a hash containing attachments.
        if msg.respond_to?(:gsub)
          msg.gsub('<b>', '*').gsub('</b>', '*').gsub('<br>', '*')
        else
          msg
        end
      end
      output_messages
    end
  end
end
