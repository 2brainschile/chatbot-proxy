module HandlerService
  module WhatsappUtils
    def format_bot_response(handle_params)
      opts = { body: {}, code: 200 }

      messages = []
      @bot_response[:body][:result][:fulfillment][:messages].each do |msg|
        messages << msg[:speech] if msg[:type] != 3 # 3 = image
      end
      opts[:body] = { response: messages, ignore: handle_params['ignore'] || false  }
      opts
    end
  end
end
