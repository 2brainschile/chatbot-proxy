# Helper module which provides common methods to Handler class
module HandlerService
  # Module which provides respones for success and error cases
  module ResponseStatus
    def message_error(opts = {})
      { body: opts[:status], status: opts[:code] }
    end

    def message_success(opts = { body: 'ok', code: '200' })
      status = opts[:code] || opts [:status]
      { body: opts[:body], status: status }
    end

    def define_response_message(response)
      if response[:code] == 200
        message_success(response)
      else
        message_error(response)
      end
    end
  end
end
