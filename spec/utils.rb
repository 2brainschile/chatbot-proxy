module Utils
  def load_json(file)
    JSON.parse(File.read(Rails.root.join("spec/fixtures/#{file}")))
  end

  def load_firebase_json(file)
    JSON.parse(File.read(Rails.root.join("spec/support/fixtures/firebase/#{file}")))
  end

  def load_web_app_json(file)
    JSON.parse(File.read(Rails.root.join("spec/support/fixtures/web_app/#{file}")))
  end
end
