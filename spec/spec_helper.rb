require 'simplecov'
require 'webmock/rspec'
SimpleCov.start 'rails'
WebMock.disable_net_connect!(allow_localhost: true)
RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.before(:each) do
    #stub_request(:any, /testdb-67a62.firebaseio.com/).to_rack(FakeFirebase)
    stub_request(:any, /smartbots-62619.firebaseio.com/).to_rack(FakeFirebase)
    stub_request(:any, /graph.facebook.com/).to_rack(FakeFacebook)
    stub_request(:any, /api.api.ai/).to_rack(FakeApiai)
    stub_request(:any, /slack.com/).to_rack(FakeSlack)
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
