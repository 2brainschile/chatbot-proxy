require 'rails_helper'

describe FacebookService::Helpers do
  it'retreives fanpage name based on fanpage_id' do
    p_a_t = ENV['PAGE_ACCESS_TOKEN']
    v_t   = ENV['VERIFY_TOKEN']
    service_data = { keys: { PAGE_ACCESS_TOKEN: p_a_t,
                             VERIFY_TOKEN: v_t } }
    id = FacebookService::Client.new(service_data).fanpage_id
    expect(id).to eq('1137725463')
  end
end
