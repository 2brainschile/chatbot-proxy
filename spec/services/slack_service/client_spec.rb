require 'rails_helper'

describe SlackService::Client do
  it 'succesfuly sends messages to multiple recipents' do
    token = 'xoxb-152636694036-jFpIOUDvnZ78nhX4xlyl5B1U'
    client = SlackService::Client.new(token, 'Test bot')
    resp = client.create_new_message('Alguien comenzó a hablar con el bot', 
                                     { 'name': 'Javier',
                                       'email': 'javier@javier.com' })

    expect(resp).to eq(true)
  end

  it 'correctly builds attachment data' do
    token = 'xoxb-152636694036-jFpIOUDvnZ78nhX4xlyl5B1U'
    client = SlackService::Client.new(token, 'Test bot')
    bot_name = client.bot_name

    att = client.build_full_attachments(field_title: 'Mensaje de Test bot',
                                        fields: { 'name': 'test_name',
                                                  'email': 'test@email.com' })
    expect(att[0][:fallback]).to eq('Mensaje de Test bot')
    expect(att[0][:author_name]).to eq(bot_name)
    expect(att[0][:footer]).to eq(bot_name)
    expect(att[0][:fields][0][:title]).to eq('Name')
    expect(att[0][:fields][0][:value]).to eq('test_name')
    expect(att[0][:fields][1][:title]).to eq('Email')
    expect(att[0][:fields][1][:value]).to eq('test@email.com')
  end
end
