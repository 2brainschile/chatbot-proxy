require 'sinatra/base'

class FakeSlack < Sinatra::Base
  post '/api/chat.postMessage' do
    json_response(200, 'post_message_success.json')
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/slack/' + file_name, 'rb').read
  end
end
