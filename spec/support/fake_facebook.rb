require 'sinatra/base'

class FakeFacebook < Sinatra::Base
  get '/v2.10/me?fields=id&access_token=:token' do
    json_response(200, 'facebook_id.json')
  end

  get '/v2.10/:user_id' do
    json_response(200, 'facebook_profile_id.json')
  end

  post '/v2.10/me/messages' do
    json_response(200, 'facebook_message.json')
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/facebook/' + file_name, 'rb').read
  end
end
