require 'sinatra/base'

class FakeApiai < Sinatra::Base

  post '/v1/query' do
    json_response(200, 'welcome.json')
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/apiai/' + file_name, 'rb').read
  end
end
