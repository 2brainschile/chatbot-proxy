require 'sinatra/base'

class FakeFirebase < Sinatra::Base
  get '/search_project_id/1829051480646588.json' do
    json_response(200, 'smartbots-62619-1829051480646588-export.json')
  end

  get '/search_project_id/cb2188f2b9099e858a0747330608037a52a756427f7a96ef.json' do
    json_response(200, 'smartbots-62619-cb2188f2b9099e858a0747330608037a52a756427f7a96ef-export.json')
  end

  get '/search_project_id/1906992709621151.json' do
    json_response(200, 'smartbots-62619-1906992709621151-export.json')
  end

  get '/search_project_id/4bb4cc46acec43fd9e875c40286062af.json' do
       json_response(200, 'smartbots-62619-services-export.json')
  end

  get '/projects/parque_oasis/services.json' do
    json_response(200, 'smartbots-parque-oasis-services.json')
  end

  get '/projects/parque_oasis_test/services.json' do
    json_response(200, 'smartbots-62619-1906992709621151-export.json')
  end

  get '/parque_oasis_keys.json' do
    json_response(200, 'smartbots-62619-parque_oasis_keys-export.json')
  end

  get '/projects/ERROR/services.json' do
    json_response(200, 'smartbots-parque-oasis-services-failed.json')
  end

  get '/search_project_id/fake_token.json' do
    json_response(200, 'smartbots-62619-fake_token-export.json')
  end

  get '/_keys.json' do
    json_response(200, 'smartbots-62619-_keys-export.json')
  end

  post '/chats/:app_id/:user_id/messages.json' do
    json_response(200, 'smartbots-add_message-export.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/1267328156654128.json' do
    json_response(200, 'smartbots-add_message-export.json')
  end

  put '/chatrooms/53452177950043fab9c032727abfd6bd/1267328156654128.json' do
    json_response(200, 'smartbots-add_message-export.json')
  end

  post '/chats/53452177950043fab9c032727abfd6bd.json' do
    json_response(200, 'push_chatroom_response.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/1267328156654128/lastMessage.json' do
    json_response(200, 'smartbots-last-message.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/1493666557350459/lastMessage.json' do
    json_response(200, 'smartbots-last-message.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/psbk3ZXEX/lastMessage.json' do
    json_response(200, 'smartbots-last-message.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/c2JzdG4uam1uekB5YWhvby5jb20/lastMessage.json' do
    json_response(200, 'smartbots-last-message.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/c2JzdG4uam1uekB5YWhvby5jb20.json' do
    json_response(200, 'smartbots-new-webapp-chatroom.json')
  end

  put '/chats/53452177950043fab9c032727abfd6bd/c2JzdG4uam1uekB5YWhvby5jb20=.json' do
    json_response(200, 'smartbots-new-webapp-chatroom.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/psbk3ZXEX/userName.json' do
    json_response(200, 'userName.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/psbk3ZXEX/userEmail.json' do
    json_response(200, 'userEmail.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/psbk3ZXEX/userPhone.json' do
    json_response(200, 'userPhone.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/1493666557350459/manualMode/active.json' do
    json_response(200, 'manualMode.json')
  end

  get '/chatrooms/53452177950043fab9c032727abfd6bd/1493666557350459.json' do
    json_response(200, 'facebook-chatroom.json')
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/firebase/' + file_name, 'rb').read
  end
end
