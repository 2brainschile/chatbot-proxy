require 'rails_helper'

RSpec.describe WebApp::ChatRoomsController, type: :controller do

  describe 'POST #create' do
    it 'returns http success' do
      body          = load_json('webform_params.json')
      response_json = load_firebase_json('set_chatroom_response_webapp.json')

      post :create, params: body

      expect(response).to have_http_status(:success)
      expect(response.body).to eq(response_json.to_json)
    end
  end
end
