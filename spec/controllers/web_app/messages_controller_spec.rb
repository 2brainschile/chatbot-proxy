require 'rails_helper'

RSpec.describe WebApp::MessagesController, type: :controller do

  describe 'POST #create' do
    it 'returns http success' do
      body     = load_web_app_json('incomming_message.json')
      post :create, params: body

      expect(response).to have_http_status(:success)
    end
  end
end
