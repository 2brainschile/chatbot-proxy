require 'rails_helper'

RSpec.describe Messenger::WidgetController, type: :controller do
  describe 'GET#index' do
    it 'returns http success when tokens match' do
      query = {
        'hub.mode': 'subscribe',
        'hub.challenge': '1167096314',
        'hub.verify_token': 'cb2188f2b9099e858a0747330608037a52a756427f7a96ef',
        format: :json
      }
      get :index, params: query
      expect(response).to have_http_status(:success)
      expect(response.body).to eq(query[:"hub.challenge"])
    end

    it 'returns http forbiden when tokens don\'t match' do
      query = {
        'hub.mode': 'subscribe',
        'hub.challenge': '1167096314',
        'hub.verify_token': 'fake_token',
        format: :json
      }
      get :index, params: query
      expect(response).to have_http_status(403)
    end
  end

  describe 'POST#create' do
    it 'returns Welcome response when WELCOME event is received' do
      get_started_message = load_json('get_started_facebook.json')
      post :create, params: get_started_message
      expect(response).to have_http_status(:success)
    end

    it 'returns Welcome response when FACEBOOK_WELCOME event is received' do
      get_started_message = load_json('get_started_facebook2.json')
      post :create, params: get_started_message
      expect(response).to have_http_status(:success)
    end

    it 'returns API AI response when normal message is received' do
      normal_message = load_json('incomming_fb_message.json')
      post :create, params: normal_message
      expect(response).to have_http_status(:success)
    end

    it 'Keeps running when like image is sent' do
      like_message = load_json('like_message.json')
      post :create, params: like_message
      expect(response).to have_http_status(:success)
      expect(response.body).to eq('No message sent')
    end
  end
end
