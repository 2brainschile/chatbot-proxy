require 'rails_helper'

RSpec.describe Facebook::ContactCenterController, type: :controller do
  describe "POST #create" do
    it 'forwards the message to facebook and firebase' do
      cc_message = load_json('post_contact_center.json')

      request.env["CONTENT_TYPE"] = "application/json"
      request.env["ACCEPT"] = "application/json"

      post :create, params: cc_message
      expect(response).to have_http_status(:success)
    end
  end
end
