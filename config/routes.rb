# This file dinamicaly generates REST endpoints
Rails.application.routes.draw do
  root 'index#index'
  get '/_ah/health' => 'index#index'

  match '*all', to: proc { [204, {}, ['']] }, via: :options

  namespace :web_app do
    resources :chat_rooms, only: :create
    resources :messages, only: :create
    post 'contact_center/broadcast', to: 'contact_center#broadcast'
    post 'contact_center/notification', to: 'contact_center#notification'
    get 'schedule/:chatbot_id' => 'schedule#working'
  end

  namespace :chrome_ext do
    resources :chat_rooms, only: :create
    resources :messages, only: :create
  end

  namespace :facebook do
    resources :messenger, only: %i[index create]
    resources :contact_center, only: :create
  end

  namespace :messenger do
    resources :widget, only: %i[index create]
    resources :contact_center, only: :create
  end

  namespace :whatsapp do
    resources :messages, only: :create
    resources :contact_center, only: :create
    post 'notification', to: 'contact_center#notification'
  end

  namespace :statistics do
    resources :basic_statistics, only: :show
    resources :bot_statistics, only: :show
  end

  namespace :google_hangouts do
    resources :messages, only: :create
  end

  namespace :settings do
    resources :intents, only: %i[index show create update destroy]
  end

  namespace :komatsu do
    resources :tickets, only: %i[create]
  end
end
