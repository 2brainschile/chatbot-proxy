# rake fix:facebook_users CHATBOT_ID=XX FANPAGE_ID=XX FB_PAGE_ACCESS_TOKEN=XX
namespace :fix do
  desc "Fix facebook users data on firebase"
  task :facebook_users => :environment do
    firebase = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
    chats = firebase.get("chats/#{ENV['CHATBOT_ID']}")
    if chats.success?
      chats.body.each do |sender_id, chat|
        if chat['userName'].nil?
          puts "Fixing user id=#{sender_id} data"
          # Get username from facebook
          page_access_token = ENV['FB_PAGE_ACCESS_TOKEN']
          profile_info = HTTParty.get("https://graph.facebook.com/v2.10/#{sender_id}?fields=first_name,last_name&access_token=#{page_access_token}")
          
          user_name = profile_info.parsed_response['first_name']
          
          opts = { createdAt: Firebase::ServerValue::TIMESTAMP,
                   userName:  user_name,
                   parseDate: DateTime.now.to_s,
                   appId:     ENV['FANPAGE_ID'],
                   userId:    sender_id,
                   origin:    'facebook' }
          
          firebase_client = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
          firebase_client.update("chats/#{ENV['CHATBOT_ID']}/#{sender_id}", opts)
        end
      end
    end
  end
end
  