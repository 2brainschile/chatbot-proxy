# rake export:conversations CHATBOT_ID=""
namespace :export do
  desc "Exports conversations in CSV"
  task :conversations => :environment do
  firebase = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
  chats = firebase.get("chats/#{ENV['CHATBOT_ID']}")
  if chats.success?
    append_conversation_headers
    chats.body.each do |chat_id, chat|
      chatroom = firebase.get("chatrooms/#{ENV['CHATBOT_ID']}/#{chat_id}")
      chatroom_detail = chatroom.body
      next if chatroom_detail.nil?
      user_detail = {
        id: chatroom_detail['userId'],
        name: chatroom_detail['userName'],
        email: chatroom_detail['userEmail'],
        phone: chatroom_detail['userPhone'],
        origin: chatroom_detail['origin'],
      }
      # append_conversation_detail user_detail, chatroom_detail['createdAt']
      if chat["messages"] && !tests_emails.include?(chatroom_detail[:email])
        chat["messages"].each do |conversation_id, conversation|
          if conversation['message']['speech'] && !conversation['message']['ignore']
            #puts "#{conversation['sender']}: #{conversation['message']['speech']}"
            message = conversation['message']['speech']
          else
            #puts "#{conversation['sender']}: #{conversation['message']}"
            message = conversation['message']
          end
          if message['payload'] || message['type']
            if message['payload'] && message['payload']['custom_message']
              message = message['payload']['custom_message']['text']
            else
              next
            end
          end
          if not message.is_a?(String)
            next
          end

          messageDate = conversation['createdAt'] || conversation['sendAt']
          messageDate = Time.at((messageDate/1000)).to_datetime.strftime('%I:%M%p %d/%m/%Y')
          conversationDate = Time.at((chatroom_detail['createdAt']/1000)).to_datetime.strftime('%I:%M%p %d/%m/%Y')
          append_conversation_to_csv conversation['sender'], message, user_detail, messageDate, conversationDate
        end
      end
    end
  end
  end

  def tests_emails
    ['javierdh6@gmail.com', 'test@test.com']
  end


def append_conversation_headers
    CSV.open("conversations_#{ENV['CHATBOT_ID']}.csv", "a+") do |csv|
      csv << ["ID", "NOMBRE", "CORREO", "FONO", "ORIGEN", "FECHA CREACION", "EMISOR", "FECHA/HORA CONVERSACION", "TEXTO"]
  end
end

  def append_conversation_to_csv sender, text, user, message_date, createdAt
    CSV.open("conversations_#{ENV['CHATBOT_ID']}.csv", "a+") do |csv|
    csv << [user[:id], user[:name], user[:email], user[:phone], user[:origin], createdAt, sender, message_date, text]
  end
end

end
#rake export:conversations CHATBOT_ID="4bb4cc46acec43fd9e875c40286062af"
