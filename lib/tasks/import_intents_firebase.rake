# rake import:intents_firebase BOT_ID="" DEV_TOKEN=""
namespace :import do
    desc "Import API.AI Intents to Firebase"
    task :intents_firebase => :environment do

      def firebase
        Firebase::Client.new(ENV['FIREBASE_APP_URL'])
      end

      def dialog_base_url
        "https://api.dialogflow.com/v1"
      end

      def dialogflow_header
        {
          'Authorization': "Bearer #{ENV['DEV_TOKEN']}",
          'Content-Type': 'application/json; charset=utf-8'
        }
      end

      def get_all_intents
        HTTParty.get("#{dialog_base_url}/intents?v=20150910", headers: dialogflow_header)
      end


      def get_intent intent_id
        HTTParty.get("#{dialog_base_url}/intents/#{intent_id}?v=20150910", headers: dialogflow_header)
      end
    

      all_intents = get_all_intents
      if all_intents.success?
        ap all_intents.parsed_response
        all_intents.each do |intent|
          intent_detail = get_intent intent['id']
          if intent_detail.success?
            intent['detail'] = intent_detail
            firebase.set("intents/#{ENV['BOT_ID']}/#{intent['id']}", intent)
            sleep 0.5
          end
        end
      end



    end
end
