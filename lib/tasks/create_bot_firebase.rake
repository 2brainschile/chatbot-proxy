# rake create:firebase_bot API_CLIENT_TOKEN=XX CHATBOT_NAME=""
namespace :create do
  desc "Create bot in FiREBASE"
  task :firebase_bot => :environment do
    firebase = Firebase::Client.new(ENV['FIREBASE_APP_URL'])
    services = { 
      api_ai: {
        keys: {
          "API_AI_ID": ENV['API_CLIENT_TOKEN']
        }
      },
      slack:{
        keys: {
          "TOKEN": ENV['SLACK_API_TOKEN']
        },
        recipents:{
          "#{ENV['DEFAULT_LOGGER_USER_ID']}": "Smartbots logger"
        }
      }
    }
    if ENV['PAGE_ACCESS_TOKEN']
      services['facebook'] = {
        keys: {
          "PAGE_ACCESS_TOKEN": ENV['PAGE_ACCESS_TOKEN'],
          "VERIFY_TOKEN": ENV['VERIFY_TOKEN'], 
        }
      }
    end
    response = firebase.set("projects/#{ENV['CHATBOT_NAME']}", {services: services})
    if response.success?
      puts "PROYECTO AGREGADO A FIREBASE: projects"
      search_project = firebase.set("search_project_id/#{ENV['API_CLIENT_TOKEN']}", ENV['CHATBOT_NAME'])
      if search_project.success?
        puts "PROYECTO AGREGADO A FIREBASE: search_project_id"
      else
        puts "Error in set search_project_id"
        puts search_project.body
      end
    else
      puts "Error in set projects"
      puts response.body
    end
  end
end
